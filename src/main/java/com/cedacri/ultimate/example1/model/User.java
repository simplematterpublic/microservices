package com.cedacri.ultimate.example1.model;

public class User {

    private String name;
    private String firstName;
    private String lastName;

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public String getName() {

        return name;
    }

    public String getFirstName() {

        return firstName;
    }

    public String getLastName() {

        return lastName;
    }
}
