package com.cedacri.ultimate.example1.dao;

import com.cedacri.ultimate.example1.model.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserDao {

    private final Map<String, User> users;

    public UserDao() {

        this.users = new HashMap<>();
    }

    public void put(final User user) {

        users.put(user.getName(), user);
    }

    public User get(final String name) {

        return users.get(name);
    }

    public Collection<User> all() {

        return users.values();
    }
}
