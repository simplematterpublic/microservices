package com.cedacri.ultimate.example1;

import com.cedacri.ultimate.example1.dao.UserDao;
import com.cedacri.ultimate.example1.model.User;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.Collection;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);
    private final UserDao userDao;

    private User createUser() {

        final User user = new User();
        user.setName("srocco");
        user.setFirstName("Stefano");
        user.setLastName("Rocco");
        return user;
    }

    public MainVerticle() {

        this.userDao = new UserDao();
        userDao.put(createUser());
    }

    @Override
    public void start(final Promise<Void> promise) {

        final int port = 9000;

        final HttpServer server = vertx.createHttpServer();
        final Router router = Router.router(vertx);
        router.get("/hello").handler(this::helloAll);
        router.get("/hello/:name").handler(this::helloName);
        router.put("/users.json/:name").handler(this::putUser);
        router.get("/users.json/all").handler(this::getUsers);
        router.get("/users.json/:name").handler(this::getUser);

        server.requestHandler(router).listen(9000, asyncResult -> {
            if (asyncResult.succeeded()) {
                LOGGER.info("HTTP server running on port " + port);
                promise.complete();
            } else {
                LOGGER.error("Could not start a HTTP server", asyncResult.cause());
                promise.fail(asyncResult.cause());
            }
        });
    }

    private void indexHandler(final RoutingContext context) {

        context.response().putHeader("Content-Type", "text/html");
        context.response().end("Home");
    }

    private void helloAll(final RoutingContext context) {

        context.response().putHeader("Content-Type", "text/html");
        context.response().end("hello all");
    }

    private void helloName(final RoutingContext context) {

        final String name = context.request().getParam("name");
        context.response().putHeader("Content-Type", "text/html");
        context.response().end("hello " + name);
    }

    private void putUser(final RoutingContext context) {

        final String name = context.request().getParam("name");

    }

    private void getUser(final RoutingContext context) {

        final String name = context.request().getParam("name");
        final User user = userDao.get(name);
        if (user != null) {
            final JsonObject json = new JsonObject();
            json.put("name", name);
            json.put("firstName", user.getFirstName());
            json.put("lastName", user.getLastName());
            context.response().putHeader("Content-Type", "application/json");
            context.response().end(json.toString());
        } else {
            context.response().setStatusCode(404);
            context.response().end();
        }
    }

    private void getUsers(final RoutingContext context) {

        final JsonArray jsonArray = new JsonArray();
        final Collection<User> users = userDao.all();
        users.forEach(user -> {
            final JsonObject json = new JsonObject();
            json.put("name", user.getName());
            json.put("firstName", user.getFirstName());
            json.put("lastName", user.getLastName());
            jsonArray.add(json);
        });
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(jsonArray.toString());
    }

    public static void main(String[] args) {

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new MainVerticle());
    }
}
